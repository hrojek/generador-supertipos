GHC = ghc -threaded --make
GHCdebug = $(GHC) -Wall -threaded --make $(DEBUG) #$(PROFILING)
C   = .chosyn.cfg
DEBUG  = -auto-all -caf-all -rtsopts
PROFILING  = -prof -auto-all -caf-all
# change below to the actual directory of hkc and petrify
HKCDIR = ./hknt-1.0
PETRYDIR = ../petrify/bin

all:
	$(GHC) gmc.hs &&\
	$(GHC) BuildGlobal.hs &&\
	$(GHC) GGparser.hs &&\
	$(GHC) SystemParser.hs &&\
	$(GHC) sgg.hs &&\
	$(GHC) sysparser.hs

debug:
	$(GHCdebug) gmc.hs &&\
	$(GHCdebug) BuildGlobal.hs &&\
	$(GHCdebug) GGparser.hs &&\
	$(GHCdebug) SystemParser.hs &&\
	$(GHCdebug) sgg.hs &&\
	$(GHCdebug) sysparser.hs

# To get the stack trace, add +RTS -xc at the end of the gmc or BuildGlobal command
prof:
	$(GHC) $(PROFILING) gmc.hs && ghc --make  $(PROFILING) BuildGlobal.hs

clean:
	rm -f *.o *.hi temp1 temp2 tempefc *~

parser:
	happy -a -i  GGGrammar.y -o GGparser.hs && $(GHC) GGparser.hs
	happy -a -i  SystemGrammar.y -o SystemParser.hs && $(GHC) SystemParser.hs

config:
	echo "hkc " $(HKCDIR)/hkc > $(C)
	echo "petry  " $(PETRYDIR)/petrify >> $(C)
#	echo "hkc ./hknt-1.0/hkc" > $(C)
#	echo "petry  ~/Dropbox/mypapers/stpn/tool/petrify/bin" >> $(C)
	echo "gmc ./gmc" >> $(C)
	echo "bg ./BuildGlobal" >> $(C)
	make hp

tbc:
	grep TODO *.hs && cat TODO

hp:
	if test -x $(HKCDIR)/hkc; then echo "The binary of hkc is already there"; else make -C $(HKCDIR); fi
	if test -h $(HKCDIR)/hkc$(shell uname -s); then echo "The link to hkc is already there"; else (cd $(HKCDIR); ln -s hkc hkc$(shell uname -s)) ; fi
	if test -h $(PETRYDIR)/petrify$(shell uname -s); then echo "The link to petrify is already there"; else (cd $(PETRYDIR); ln -s petrify petrify$(shell uname -s)); fi
