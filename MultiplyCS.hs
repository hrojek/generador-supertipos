module MultiplyCS where

import Prelude hiding (succ)
import Data.Set as S
import Data.List as L
import CFSM


--
-- This module is useful to generate bigger systems
--

multiply ::  Int -> System_P -> System_P
multiply i mysys@(sys,_)
    | i == 0 = mysys
    | otherwise = ((fst sys1) ++ (fst sys2), (snd sys1) ++ (snd sys2))
    where sys1 = multiply (i-1) mysys
          sys2 = indexCS (i* length sys) mysys

indexCS :: Int -> System_P -> System_P
indexCS i (sys, participant) = (L.map (\x -> indexCFSM i x) sys, L.map (\x -> x ++ "_" ++ (show i)) participant)

indexCFSM :: Int -> CFSM_P -> CFSM_P
indexCFSM i (states, q0, acts, trans) = (states, q0, newacts , newtrans)
    where newacts                      = S.map updateaction acts
          newtrans                     = S.map (\(x,y,z) -> (x, updateaction y, z)) trans
          updateaction (dir,(s,r),msg) = (dir, (s + i, r+i), msg)
        
