#!/usr/bin/python
import sys
import subprocess
import os
import os.path
import string
import time
import numpy as np
import csv


directory = "./tests/benchmark/gmc/"
iterations = 1           # SET THIS TO '30' to generate proper data
multiple = 2             # SET THIS TO '2' to generate bigger protocols
files = os.listdir(directory)
matrix =  [[0 for x in xrange(iterations+1)] for x in xrange(len(files))]

for factor in range(multiple):
    print "#############################################################"
    print "                     ", factor
    print "#############################################################"
    k = 0
    for f in files:
        matrix[k][0] = f
        k += 1
        
    for i in range(1,iterations+1):
        j = 0
        for f in files:
            print "[",i,"] ", "Processing: ", f
            startTime = time.time()
            subprocess.call(["./rungmc.py",directory+f,str(factor)])
            endTime = time.time()
            matrix[j][i] =  endTime - startTime
            j += 1

    output = [[0 for x in xrange(2)] for x in xrange(len(files))]
    j = 0
    for i in files:
        output[j][0] = matrix[j][0]
        output[j][1] = np.mean(matrix[j][1:])
        print matrix[j][0], ": ", np.mean(matrix[j][1:])
        j+=1

    name = "benchmark_result"+str(factor)+".csv"
    with open(name,"wb") as out:
        write = csv.writer(out)
        write.writerows(output)
